from django.urls import path
from rest_framework import routers

from app import views as rest_views


router = routers.SimpleRouter()
router.register('project', rest_views.ProjectViewSet)
router.register('tasks', rest_views.TasksViewSet)
router.register(r'users', rest_views.UserViewSet)
urlpatterns = router.urls + [
    path('change-password/', rest_views.ChangePasswordView.as_view(), name='change-password'),
    path('register/', rest_views.RegisterView.as_view(), name='register'),
]
