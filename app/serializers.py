from rest_framework import serializers
from .models import Project, Tasks, CustomUser


class UserSerializer(serializers.HyperlinkedModelSerializer):
    password = serializers.CharField(max_length=250, write_only=True)

    class Meta:
        model = CustomUser
        fields = ('username', 'password', 'email', 'first_name', 'last_name',)


class TasksSerializerForProject(serializers.ModelSerializer):
    class Meta:
        model = Tasks
        fields = ['id', 'title']


class ProjectSerializer(serializers.ModelSerializer):
    list_of_tasks = TasksSerializerForProject(many=True)

    class Meta:
        model = Project
        fields = ['id', 'title', 'description', 'user', 'color', 'total_time', 'list_of_tasks']


class TasksSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tasks
        fields = ['id', 'title', 'description', 'start_time', 'finish_time', 'status']


class ChangePasswordSerializer(serializers.Serializer):
    model = CustomUser
    old_password = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)


class RegisterSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)
    password2 = serializers.CharField(write_only=True)

    class Meta:
        model = CustomUser
        fields = ['first_name', 'last_name', 'phone', 'username', 'email', 'password', 'password2']

    def validate(self, attrs):
        if attrs['password'] != attrs['password2'] or len(attrs['password']) < 8:
            raise Exception("Don't match")
        return attrs

    def create(self, validated_data):
        password = validated_data.pop('password')
        validated_data.pop('password2')
        user = CustomUser(**validated_data)
        user.set_password(password)
        user.save()
        return user
