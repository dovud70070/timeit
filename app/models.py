from django.db import models
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.utils.text import gettext_lazy as _
from .managers import UserManager
from django.core.mail import send_mail


class CustomUser(AbstractUser):
    username_validator = UnicodeUsernameValidator()

    username = models.CharField(_('username'), max_length=150, unique=True, null=True, blank=True,
                                help_text=_('Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
                                validators=[username_validator],
                                error_messages={'unique': _('A user with that username already exists.')})
    first_name = models.CharField(_('first name'), max_length=150, blank=True)
    last_name = models.CharField(_('first name'), max_length=150, blank=True)
    dob = models.DateField(null=True)
    phone = models.CharField(
        max_length=150,
        null=True,
        blank=True,
        unique=True,
        error_messages={
            'unique': _('A user with that phone already exists.')
        },
    )
    email = models.EmailField(
        _('email address'),
        blank=True,
        unique=True,
        error_messages={
            'unique': _("A user with that email already exists.")
        },
    )
    image = models.ImageField(upload_to=f'user_profile_image/', null=True, blank=True)
    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('Designates whether the user can log into this admin site.'),
    )
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )
    date_joined = models.DateTimeField(_('date joined'), auto_now_add=True)
    position = models.CharField(max_length=250, null=True, blank=True)
    working_company_logo = models.ImageField(null=True, blank=True)
    experience = models.FloatField(null=True, blank=True)
    rate = models.FloatField(null=True, blank=True)
    info = models.TextField(null=True, blank=True)

    objects = UserManager()

    EMAIL_FIELD = 'email'
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def clean(self):
        super().clean()
        self.email = self.__class__.objects.normalize_email(self.email)

    def get_full_name(self):
        """
        Return the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        """Return the short name for the user."""
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        """Send an email to this user."""
        send_mail(subject, message, from_email, [self.email], **kwargs)


class Project(models.Model):
    WHITE = 'white',
    BLACK = 'black'
    COLORS = (
        ('WHITE', 'White'),
        ('BLACK', 'Black')
    )
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE, null=True, blank=True)
    title = models.CharField(max_length=250, null=True, blank=True)
    slug = models.SlugField(max_length=250, null=True, blank=True)
    color = models.CharField(max_length=250, choices=COLORS, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    total_time = models.FloatField()
    list_of_tasks = models.ManyToManyField('Tasks', blank=True, related_name='tasks')

    def __str__(self):
        return self.title


class Tasks(models.Model):
    OPEN = 'open'
    FINISHED = 'finished'
    STATUS = (
        (OPEN, 'Open'),
        (FINISHED, 'Finished')
    )
    title = models.CharField(max_length=250, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    start_time = models.DateField(null=True, blank=True)
    finish_time = models.DateField(null=True, blank=True)
    status = models.CharField(max_length=250, choices=STATUS, null=True, blank=True)

    def __str__(self):
        return self.title
